#!/bin/bash
ROOT=/home/hcai/
curpath=`pwd`

#:$ROOT/libs/rt.jar
MAINCP=".:$ROOT/workspace_sireum/collusion/bin:/home/hcai/Sireum/otherlibs/apktool.jar"

export SIREUM_HOME=/home/hcai/Sireum

for mod in `ls /home/hcai/workspace_sireum/`
do
	for module in /home/hcai/workspace_sireum/$mod/*
	do
		#module=`readlink -f $module`
		#echo "module="$module
		if [ ! -d "$module" ];then 
			continue
		fi
		MAINCP=$MAINCP:$module/bin
		if [ ! -d "$module"/libs ];then 
			continue
		fi
		for i in $module/libs/*.jar;
		do
			MAINCP=$MAINCP:$i
		done
	done
done
for j in /home/hcai/Sireum/otherlibs/*.jar
do
	MAINCP=$MAINCP:$j
done

: << "com"
for j in /home/hcai/workspace_sireum/prelude/sireum-lib/lib/*.jar
do
	MAINCP=$MAINCP:$j
done
for j in /home/hcai/workspace_sireum/prelude/sireum-lib/elib/*.jar
do
	MAINCP=$MAINCP:$j
done
for j in /home/hcai/workspace_sireum/jawa/sireum-jawa/lib/*.jar
do
	MAINCP=$MAINCP:$j
done
for j in /home/hcai/workspace_sireum/amandroid/sireum-amandroid/lib/*.jar
do
	MAINCP=$MAINCP:$j
done
com

starttime=`date +%s%N | cut -b1-13`

srcpath=/home/hcai/Downloads/DroidBench-master/testedapk
destpath=$curpath/.temp
mkdir -p $destpath
#scala -J-Xmx6g -classpath ${MAINCP} org.sireum.amandroid.run.collusion.ICCFlow_run \
scala -J-Xmx6g -classpath ${MAINCP} singleapp.ICCAnalysis \
	$srcpath \
	$destpath 

stoptime=`date +%s%N | cut -b1-13`
echo "time elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0

# hcai vim :set ts=4 tw=4 sws=4
