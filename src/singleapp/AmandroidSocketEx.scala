/**
 * File: src/singleapp/AmandroidSocketEx.scala
 * -------------------------------------------------------------------------------------------
 * Date     Author      Changes
 * -------------------------------------------------------------------------------------------
 * 11/12/15   hcai      created; customize AmandroidSocket for performance (avoid loading and analyzing 
 *                      3-party libraries; avoid decompiling for apks that have been done so previously
*/
package singleapp

import org.sireum.util.FileUtil
import org.sireum.amandroid.AndroidGlobalConfig
import org.sireum.jawa.alir.LibSideEffectProvider
import java.io.File
import java.net.URI
import org.sireum.jawa.util.APKFileResolver
import org.sireum.amandroid.decompile.Dex2PilarConverter
import org.sireum.amandroid.alir.pta.reachingFactsAnalysis.AndroidRFAConfig
import org.sireum.jawa.LibraryAPISummary
import org.sireum.amandroid.appInfo.AppInfoCollector
import org.sireum.amandroid.AndroidConstants
import org.sireum.amandroid.alir.pta.reachingFactsAnalysis.AndroidReachingFactsAnalysis
import org.sireum.jawa.ClassLoadManager
import org.sireum.jawa.alir.dataDependenceAnalysis.InterproceduralDataDependenceAnalysis
import org.sireum.amandroid.alir.taintAnalysis.AndroidDataDependentTaintAnalysis
import org.sireum.jawa.util.IgnoreException
import org.sireum.amandroid.alir.taintAnalysis.AndroidSourceAndSinkManager
import org.sireum.jawa.JawaMethod
import org.sireum.util.FileResourceUri
import org.sireum.jawa.alir.Context
import org.sireum.jawa.util.MyTimer
import org.sireum.jawa.ScopeManager
import org.sireum.amandroid.alir.pta.reachingFactsAnalysis.AndroidRFAScopeManager
import org.sireum.jawa.Global
import org.sireum.jawa.Constants
import org.sireum.amandroid.Apk
import org.sireum.amandroid.decompile.ApkDecompiler
import org.sireum.jawa.util.MyTimeoutException
import org.sireum.amandroid.security.AmandroidSocket
import org.sireum.amandroid.security.AmandroidSocketListener

class AmandroidSocketEx(global: GlobalEx, apk: Apk) extends AmandroidSocket(global,apk) {
  private final val TITLE = "AmandroidSocketEx"
  private var myListener_opt: Option[AmandroidSocketListener] = None
  
  override def plugListener(listener: AmandroidSocketListener): Unit = {
    super.plugListener(listener)
    myListener_opt = Some(listener)
  }
  
  override def loadApk(output_path: String, lib_sum: LibraryAPISummary, dpsuri: Option[FileResourceUri], dexLog: Boolean, debugMode: Boolean): FileResourceUri = {
    val apkFile = FileUtil.toFile(apk.nameUri)
    val name = apkFile.getName.substring(0, apkFile.getName().lastIndexOf("."))
    val resultDir = new File(output_path + "/" + name)

    var out = ""
    if (resultDir.exists()) {
      System.out.println("found " + resultDir + "; skip decompilation and dex2pilar conversion")
      out = FileUtil.toUri(resultDir)
    }
    else {
      val (_out, _) = ApkDecompiler.decompile(apkFile, resultDir, dpsuri, dexLog, debugMode, true)
      out = _out
    }
    //val (out, _) = ApkDecompiler.decompile(apkFile, resultDir, dpsuri, dexLog, debugMode, true)
    // convert the dex file to the "pilar" form
    val fileUri = out + "/src"
    if(FileUtil.toFile(fileUri).exists()) {
      /*
    	val fileUris = FileUtil.listFiles(fileUri, Constants.PILAR_FILE_EXT, true)
    	fileUris.foreach{
    		fileUri => 
    		val aspkgname = fileUri.substring(fileUri.indexOf("src/")+4).replace('/', '.')
    		if (lib_sum.isLibraryAPI(aspkgname)) {
    			if (new File(fileUri).renameTo(new File(fileUri+".skip")))
          //if (new File(fileUri).delete())
        	  System.out.println("will skip loading "+fileUri)
    		}
    	}
      */
      
      //store the app's pilar code in AmandroidCodeSource which is organized class by class.
      global.load(fileUri, Constants.PILAR_FILE_EXT, lib_sum)
    }
    out
  }
  
    def runWithDDAEx(
      ssm: AndroidSourceAndSinkManager,
      public_only: Boolean,
      parallel: Boolean,
      timer: Option[MyTimer],
      withpath : Boolean = false) = {    
    try {
      if(myListener_opt.isDefined) myListener_opt.get.onPreAnalysis
      ssm.parse(AndroidGlobalConfig.SourceAndSinkFilePath)
  
      var entryPoints = global.getEntryPoints(AndroidConstants.MAINCOMP_ENV)
  
      if(!public_only)
        entryPoints ++= global.getEntryPoints(AndroidConstants.COMP_ENV)
        
      if(myListener_opt.isDefined) 
        entryPoints = myListener_opt.get.entryPointFilter(entryPoints)
    
      ScopeManager.setScopeManager(new AndroidRFAScopeManager)
        
      {if(parallel) entryPoints.par else entryPoints}.foreach {
        ep =>
          global.reporter.echo(TITLE, "--------------Component " + ep + "--------------")
          val initialfacts = AndroidRFAConfig.getInitialFactsForMainEnvironment(ep)
          val idfg = AndroidReachingFactsAnalysis(global, apk, ep, initialfacts, new ClassLoadManager, timer)
          apk.addIDFG(ep.getDeclaringClass, idfg)
          global.reporter.echo(TITLE, "processed-->" + idfg.icfg.getProcessed.size)
          val iddResult = InterproceduralDataDependenceAnalysis(global, idfg)
          apk.addIDDG(ep.getDeclaringClass, iddResult)
          val tar = if (withpath)AndroidDataDependentTaintAnalysis(global, iddResult, idfg.ptaresult, ssm) else AndroidTaintAnalysisEx(global, iddResult, idfg.ptaresult, ssm)
          apk.addTaintAnalysisResult(ep.getDeclaringClass, tar)
      }
      if(myListener_opt.isDefined) myListener_opt.get.onAnalysisSuccess
    } catch {
      case e: Exception => 
        if(myListener_opt.isDefined) myListener_opt.get.onException(e)
    } finally {
      if(myListener_opt.isDefined) myListener_opt.get.onPostAnalysis
    }
  }
}