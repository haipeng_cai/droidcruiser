/**
 * File: src/singleapp/GatherICCSources.scala
 * -------------------------------------------------------------------------------------------
 * Date     Author      Changes
 * -------------------------------------------------------------------------------------------
 * 11/7/15   hcai       created; as the helper gather ICC sources
 * 11/13/15  hcai       largely enhanced by adding permission-based checking
*/
package singleapp

/**
 * @author hcai
 */
import org.sireum.jawa.alir.dataDependenceAnalysis._
import org.sireum.util._
import org.sireum.jawa.alir.controlFlowGraph._
import org.sireum.pilar.ast._
import org.sireum.jawa.alir.taintAnalysis._
import org.sireum.jawa.alir.interProcedural.Callee
import org.sireum.amandroid.Apk
import org.sireum.amandroid.alir.taintAnalysis.AndroidDataDependentTaintAnalysis._
import org.sireum.amandroid.alir.taintAnalysis.SourceAndSinkCategory
import org.sireum.jawa.alir.interProcedural.InterProceduralNode
import org.sireum.jawa.alir.controlFlowGraph.ICFGEntryNode

case class GatherICCSources [NodeT <: InterProceduralNode] (
        apk: Apk,
        //apiPermissions: IMap[String, ISet[String]],
        iddg: DataDependenceBaseGraph[InterproceduralDataDependenceAnalysis.Node],
        sinkNodes: ISet[TaintNode[NodeT]]) {
  
        type IDDGNodeT = InterproceduralDataDependenceAnalysis.Node

        // collect all ICC sources in the given IDDG with checking permissions
        def getAllIccSourcesRelax : ISet[NodeT] = {
         val srcs = msetEmpty[NodeT]
         iddg.nodes.foreach 
         { node =>
           node match {
             case entparaIddgNode: IDDGEntryParamNode => 
                  srcs += entparaIddgNode.asInstanceOf[NodeT]
             case entIddgNode: IDDGEntryNode =>    
                  srcs += entIddgNode.asInstanceOf[NodeT]
             case _ => 
           }
         }
         srcs.toSet
        }
        
         // collect all ICC sources in the given IDDG
        def getAllIccSources : ISet[NodeT] = {
         val srcs = msetEmpty[NodeT]
         iddg.nodes.foreach 
         { node =>
           node match {
             case entparaIddgNode: IDDGEntryParamNode => 
                  val infos = apk.getAppInfo.getComponentInfos.filter { 
                    cinfo => 
                      if (cinfo.compType.name != entparaIddgNode.getOwner.getClassName) false
                      else {
                        (cinfo.permission -- apk.getAppInfo.getUsesPermissions).isEmpty
                      }
                  }
                  if (!infos.isEmpty) srcs += entparaIddgNode.asInstanceOf[NodeT]
             case entIddgNode: IDDGEntryNode =>    
                  val infos = apk.getAppInfo.getComponentInfos.filter { 
                    cinfo => 
                      if (cinfo.compType.name != entIddgNode.getOwner.getClassName) false
                      else {
                        (cinfo.permission -- apk.getAppInfo.getUsesPermissions).isEmpty
                      }
                  }
                  if (!infos.isEmpty) srcs += entIddgNode.asInstanceOf[NodeT]
             case _ => 
           }
         }
         srcs.toSet
        }
        
        // collect all taint-sink-reachable ICC sources in the given IDDG
        def getTaintSinkReachingIccSources: ISet[TaintSource[NodeT]] = {
         val srcs = msetEmpty[TaintSource[NodeT]]
         iddg.nodes.foreach 
         { node =>
           node match {
             case entparaIddgNode: IDDGEntryParamNode => 
                if (this.isTaintSinkReachingIccSource(entparaIddgNode.asInstanceOf[NodeT])) {
                  //val tn = TaintSource(iddg.entryNode, TagTaintDescriptor(iddg.entryNode.getOwner.signature, isetEmpty, SourceAndSinkCategory.ICC_SOURCE, Set("ICC")))
                  val tn = TaintSource(entparaIddgNode.asInstanceOf[NodeT], TypeTaintDescriptor(entparaIddgNode.getOwner.signature, Some(entparaIddgNode.position), SourceAndSinkCategory.ICC_SOURCE))
                  srcs += tn
                }
             case entIddgNode: IDDGEntryNode =>    
                if (this.isTaintSinkReachingIccSource(entIddgNode.asInstanceOf[NodeT])) {
                	val tn = TaintSource(entIddgNode.asInstanceOf[NodeT], TypeTaintDescriptor(entIddgNode.getOwner.signature, None, SourceAndSinkCategory.ICC_SOURCE))
                  srcs += tn
                }
             /*
             case entIcfgNode: ICFGEntryNode =>    
                if (this.isTaintSinkReachingIccSource(entIcfgNode.asInstanceOf[NodeT])) {
                	val tn = TaintSource(entIcfgNode.asInstanceOf[NodeT], TagTaintDescriptor(entIcfgNode.getOwner.signature, isetEmpty, SourceAndSinkCategory.ICC_SOURCE, Set("ICC")))
                  srcs += tn
                }
              */
             case _ => 
           }
         }

         srcs.toSet
        }
        
        /** determine if the given node is an ICC source that is reachable to any taint sink 
         *  this version does not check permissions but only reachability as the current Amandroid does for
         *  finding tain flow paths
        */
        def isTaintSinkReachingIccSourceForResultsRelax (Node : NodeT) : (Boolean, Set[_ <: TaintNode[NodeT]]) = 
        {
          var flag = false
          var (reachableSinks, compName) = 
                  Node match {
                  case iddgEntNode: IDDGNode => 
                    (sinkNodes.filter{sinN => iddg.findPath(sinN.node.asInstanceOf[IDDGNodeT],iddgEntNode) != null},iddgEntNode.getOwner.getClassName)
                  case icfgEntNode : ICFGNode => 
                    (sinkNodes.filter{sinN => iddg.icfg.findPath(sinN.node.asInstanceOf[ICFGNode],icfgEntNode) != null},icfgEntNode.getOwner.getClassName)
                  case _ =>
                    (isetEmpty,"")
                  }
          if(!reachableSinks.isEmpty){
            flag = true
            /*
        	  val neededPermissions = reachableSinks.map(sin => sin.descriptor.asInstanceOf[TypeTaintDescriptor].tags).reduce(iunion[String])
            val infos = apk.getAppInfo.getComponentInfos
            infos.foreach{
        		  info =>
        		  if(info.compType.name == compName){
        			  if(info.exported == true){
        				  if(!info.permission.isEmpty){
        					  flag = !(neededPermissions -- info.permission).isEmpty
        				  }
        			  }
        		  }
        	  }
            */
            /*
            flag = reachableSinks.exists { rsink => 
              val infos = apk.getAppInfo.getComponentInfos.filter { cinfo => cinfo.compType.name == compName }
              infos.exists { info => 
                val neededPermission = rsink.descriptor.asInstanceOf[TypeTaintDescriptor].tags
                info.exported && 
                (neededPermission.isEmpty || (neededPermission -- info.permission).isEmpty)
                //(info.permission.isEmpty || (neededPermission -- info.permission).isEmpty)
              }
            }
            */
          }
          (flag, if (flag) reachableSinks else isetEmpty)
        }
        
        // with detailed permission check
        def isTaintSinkReachingIccSourceForResults (Node : NodeT) : (Boolean, Set[_ <: TaintNode[NodeT]]) = 
        {
          var (reachableSinks, compName) = 
       		  Node match {
       		  case iddgEntNode: IDDGNode => 
        		  (sinkNodes.filter{
                sinN => 
                if (iddg.findPath(sinN.node.asInstanceOf[IDDGNodeT],iddgEntNode) == null) false
                else {
                	val sinkcompname = sinN.node.asInstanceOf[IDDGNodeT].getOwner.getClassName
                	//val neededPermission = sinN.descriptor.asInstanceOf[TagTaintDescriptor].tags
                  // TODO: get permission tags associated with the sink as specified in the sourceSinkList
                	val neededPermission = isetEmpty[String]
                	val infos = apk.getAppInfo.getComponentInfos.filter { cinfo => cinfo.compType.name == sinkcompname}
                	var dime = !infos.isEmpty
                	infos.foreach {
                		info => 
                		dime &= ((info.permission ++ neededPermission)--apk.getAppInfo.getUsesPermissions).isEmpty
                	}
                	dime
                }
        		  },
        		  iddgEntNode.getOwner.getClassName)
       		  case icfgEntNode : ICFGNode => 
        		  (sinkNodes.filter{
                sinN => 
                if (iddg.icfg.findPath(sinN.node.asInstanceOf[ICFGNode],icfgEntNode) == null) false
                else {
                	val sinkcompname = sinN.node.asInstanceOf[ICFGNode].getOwner.getClassName
                	val neededPermission = sinN.descriptor.asInstanceOf[TagTaintDescriptor].tags
                	val infos = apk.getAppInfo.getComponentInfos.filter { cinfo => cinfo.compType.name == sinkcompname}
                	var dime = false
                	infos.foreach {
                		info => 
                		dime &= ((info.permission ++ neededPermission)--apk.getAppInfo.getUsesPermissions).isEmpty
                	}
                	dime
                }
        		  },icfgEntNode.getOwner.getClassName)
        		  case _ =>
        		  (isetEmpty,"")
          }
          (!reachableSinks.isEmpty, reachableSinks)
        }
        
        def isTaintSinkReachingIccSource (Node : NodeT) : Boolean = 
        {
         isTaintSinkReachingIccSourceForResults(Node)._1
        }
}
