/**
 * File: src/singleapp/TaintICC_run.scala
 * -------------------------------------------------------------------------------------------
 * Date     Author      Changes
 * -------------------------------------------------------------------------------------------
 * 11/7/15   hcai       created; simply combining the ICC_run and DataLeakage_run in Amandroid
*/
package singleapp

import org.sireum.amandroid.alir.pta.reachingFactsAnalysis.AndroidReachingFactsAnalysisConfig
import org.sireum.amandroid.security.AmandroidSocket
import org.sireum.util.FileUtil
import org.sireum.amandroid.security.interComponentCommunication.IccCollector
import org.sireum.amandroid.util.AndroidLibraryAPISummary
import org.sireum.amandroid.security.AmandroidSocketListener
import org.sireum.amandroid.alir.dataRecorder.DataCollector
import org.sireum.amandroid.alir.dataRecorder.MetricRepo
import org.sireum.amandroid.AndroidGlobalConfig
import java.io.PrintWriter
import java.io.File
import org.sireum.amandroid.AndroidConstants
import org.sireum.jawa.util.SubStringCounter
import org.sireum.util.FileResourceUri
import org.sireum.jawa.util.IgnoreException
import org.sireum.jawa.util.MyTimer
import org.sireum.jawa.util.MyTimeoutException
import org.sireum.jawa.Global
import org.sireum.amandroid.Apk
import org.sireum.jawa.PrintReporter
import org.sireum.jawa.MsgLevel
import org.sireum.amandroid.alir.taintAnalysis.DataLeakageAndroidSourceAndSinkManager

object TaintICC_run {
  private final val TITLE = "ICC resolution with flow analysis"
  object IccFlowCounter {
    var total = 0
    var haveresult = 0
    var haveIcc = 0
    var iccTotal = 0
    var foundIccContainer = 0
    
    var output_path = ""
    
    override def toString: String = "total: " + total + ", haveResult: " + haveresult + ", haveIcc: " + haveIcc + ", iccTotal: " + iccTotal + ", foundIccContainer: " + foundIccContainer
    
    def reset = {
      this.total = 0
      this.haveIcc = 0
      this.foundIccContainer = 0
      this.iccTotal = 0
      this.haveresult = 0
    }
  }
  
  private class IccFlowListener(global: Global, apk: Apk, app_info: IccCollector) extends AmandroidSocketListener {
    def onPreAnalysis: Unit = {
      IccFlowCounter.total += 1
      val iccSigs = AndroidConstants.getIccMethods()
      val codes = global.getApplicationClassCodes
      if(codes.exists{
        case (rName, source) =>
          iccSigs.exists(source.code.contains(_))
      })
      IccFlowCounter.haveIcc += 1
  
      codes.foreach{
        case (rName, source) =>
          IccFlowCounter.iccTotal += iccSigs.map(sig => SubStringCounter.countSubstring(source.code, sig + " @classDescriptor")).reduce((i, j) => i + j)
      }
    }

    def entryPointFilter(eps: Set[org.sireum.jawa.JawaMethod]): Set[org.sireum.jawa.JawaMethod] = {
      val res = eps.filter(e=>app_info.getIccContainers.contains(e.getDeclaringClass))
      if(!res.isEmpty){
        IccFlowCounter.foundIccContainer += 1
      }
      res
    }

    def onTimeout: Unit = {}

    def onAnalysisSuccess: Unit = {
      val appData = DataCollector.collect(global, apk)
      MetricRepo.collect(appData)
      //val outputDir = AndroidGlobalConfig.amandroid_home + "/output"
      val outputDir = IccFlowCounter.output_path + "/../output"
      val apkName = apk.nameUri.substring(apk.nameUri.lastIndexOf("/"), apk.nameUri.lastIndexOf("."))
      val appDataDirFile = new File(outputDir + "/" + apkName)
      if(!appDataDirFile.exists()) appDataDirFile.mkdirs()
      println(appData.toString())
      val out = new PrintWriter(appDataDirFile + "/AppData.txt")
      out.print(appData.toString)
      out.close()
      val mr = new PrintWriter(outputDir + "/MetricInfo.txt")
      mr.print(MetricRepo.toString)
      mr.close()
      IccFlowCounter.haveresult += 1
    }

    def onPostAnalysis: Unit = {
      global.reporter.echo(TITLE, IccFlowCounter.toString)
    }
    
    def onException(e: Exception): Unit = {
      e match{
        case ie: IgnoreException => System.err.println("Ignored!")
        case a => 
          e.printStackTrace()
      }
    }
  }
  
  def main(args: Array[String]): Unit = {
    if(args.size < 2){
      System.err.print("Usage: source_path output_path [dependence_path]")
      return
    }
    
//    GlobalConfig.ICFG_CONTEXT_K = 1
    AndroidReachingFactsAnalysisConfig.resolve_icc = false
    AndroidReachingFactsAnalysisConfig.resolve_static_init = false
//    AndroidReachingFactsAnalysisConfig.timeout = 10
    
    val sourcePath = args(0)
    val outputPath = args(1)
    val withTaint = if (args.size >= 2) args(2) else "no"
    val dpsuri = try{Some(FileUtil.toUri(args(3)))} catch {case e: Exception => None}
    val files = FileUtil.listFiles(FileUtil.toUri(sourcePath), ".apk", true).toSet
    
    IccFlowCounter.output_path = outputPath
    
    files.foreach{
      file =>
        val reporter = new PrintReporter(MsgLevel.ERROR)
        val global = new GlobalEx(file, reporter)
        global.setJavaLib(settings.config.classpath)
        val apk = new Apk(file)
        val socket = new AmandroidSocketEx(global, apk)
        try{
          IccFlowCounter.reset
          reporter.echo(TITLE, IccFlowTask(global, apk, outputPath, withTaint, dpsuri, file, socket, Some(7200)).run)   
        } catch {
          case te: MyTimeoutException => reporter.error(TITLE, te.message)
          case e: Throwable => e.printStackTrace()
        } finally{
          reporter.echo(TITLE, IccFlowCounter.toString)
          socket.cleanEnv
        }
    }
  }
  
  private case class IccFlowTask(global: GlobalEx, apk: Apk, outputPath: String, withtaint : String, dpsuri: Option[FileResourceUri], file: FileResourceUri, socket: AmandroidSocketEx, timeout: Option[Int]){
    def run: String = {
      global.reporter.echo(TITLE, "####" + file + "#####")
      val timer = timeout match {
        case Some(t) => Some(new MyTimer(t))
        case None => None
      }
      if(timer.isDefined) timer.get.start
      val outUri = socket.loadApk(outputPath, AndroidLibraryAPISummary, dpsuri, false, false)
      //global.resetLibs
      
      val app_info = new IccCollector(global, apk, outUri, timer)
      app_info.collectInfo
      socket.plugListener(new IccFlowListener(global, apk, app_info))
      if (withtaint=="yes") {
        System.out.println("ICC+Taint flow")
    	  val ssm = new DataLeakageAndroidSourceAndSinkManager(global, apk, app_info.getLayoutControls, app_info.getCallbackMethods, AndroidGlobalConfig.SourceAndSinkFilePath)
    	  socket.runWithDDAEx(ssm, false, true, timer, true)
      }
      else 
    	  socket.runWithoutDDA(false, true, timer)
      return "Done TaintICC Flow Analysis!"
    }
  }
}
