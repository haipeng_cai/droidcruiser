/**
 * File: src/singleapp/GatherICCSources.scala
 * -------------------------------------------------------------------------------------------
 * Date     Author      Changes
 * -------------------------------------------------------------------------------------------
 * 11/7/15   hcai       created; extending the Amandroid original SourceAndSinkManager to include
 *						          ICC sources
*/
package singleapp

import java.io.BufferedReader
import java.io.FileReader
import org.sireum.util._
import org.sireum.jawa.JawaMethod
import org.sireum.amandroid.parser.LayoutControl
import org.sireum.amandroid.parser.ARSCFileParser
import java.util.regex.Pattern
import java.util.regex.Matcher
import org.sireum.amandroid.AndroidConstants
import org.sireum.pilar.ast.LocationDecl
import org.sireum.jawa.alir.util.ExplicitValueFinder
import org.sireum.pilar.ast.JumpLocation
import java.io.File
import org.sireum.amandroid.alir.pta.reachingFactsAnalysis.IntentHelper
import org.sireum.jawa.alir.controlFlowGraph._
import org.sireum.jawa.alir.dataDependenceAnalysis.InterProceduralDataDependenceGraph
import org.sireum.pilar.ast._
import java.io.InputStreamReader
import java.io.FileInputStream
import org.sireum.jawa.alir.interProcedural.Callee
import org.sireum.jawa.alir.taintAnalysis.SourceAndSinkManager
import org.sireum.jawa.alir.pta.PTAResult
import org.sireum.amandroid.alir.pta.reachingFactsAnalysis.model.InterComponentCommunicationModel
import org.sireum.jawa.alir.pta.VarSlot
import org.sireum.jawa.Signature
import org.sireum.amandroid.Apk
import org.sireum.jawa.Global

import org.sireum.amandroid.alir.taintAnalysis.AndroidDataDependentTaintAnalysis._
import org.sireum.jawa.alir.dataDependenceAnalysis._
import org.sireum.amandroid.alir.taintAnalysis._
import org.sireum.jawa.alir.taintAnalysis._

class SourceAndSinkManagerEx (
    global: Global,
    apk: Apk,
    layoutControls: Map[Int, LayoutControl], 
    callbackMethods: ISet[JawaMethod], 
    sasFilePath: String,
    sinkNodes : ISet[TaintNode[Node]],
    iddg: DataDependenceBaseGraph[Node]
    ) extends DefaultAndroidSourceAndSinkManager(global, apk, layoutControls, callbackMethods, sasFilePath) {
  
  private final val TITLE = "SourceAndSinkManagerEx"
  type IDDGNodeT = InterproceduralDataDependenceAnalysis.Node
  
  override def isIccSource(entNode: ICFGNode, iddgEntNode: ICFGNode): Boolean = 
  {
    GatherICCSources(this.apk, iddg, sinkNodes).isTaintSinkReachingIccSource(entNode.asInstanceOf[IDDGNodeT])
  }
}

class SourceAndSinkManagerRelax (
    global: Global,
    apk: Apk,
    layoutControls: Map[Int, LayoutControl], 
    callbackMethods: ISet[JawaMethod], 
    sasFilePath: String) extends DataLeakageAndroidSourceAndSinkManager(global, apk, layoutControls, callbackMethods, sasFilePath) {
  
  private final val TITLE = "SourceAndSinkManagerRelax"
  
  // this relax overloading is just to get all ICC sources regardless of being reachable to any taint sinks or not
  override def isIccSource(entNode: ICFGNode, iddgEntNode: ICFGNode): Boolean = true
}

class SourceAndSinkManagerStrict (
    global: Global,
    apk: Apk,
    layoutControls: Map[Int, LayoutControl], 
    callbackMethods: ISet[JawaMethod], 
    sasFilePath: String) extends DataLeakageAndroidSourceAndSinkManager(global, apk, layoutControls, callbackMethods, sasFilePath) {
  
  private final val TITLE = "SourceAndSinkManagerStrict"
  
  // this relax overloading is just to get all ICC sources regardless of being reachable to any taint sinks or not
  override def isIccSource(entNode: ICFGNode, iddgEntNode: ICFGNode): Boolean = false
  
  def getSourceTags(calleep: JawaMethod): ISet[String] = {
    this.sources.filter(_._1 == calleep.getSignature.signature.replaceAll("\\*", "")).map(_._2).fold(isetEmpty)(iunion _)
  }
  
  def getSinkTags(calleep: JawaMethod): ISet[String] = {
    this.sinks.filter(_._1 == calleep.getSignature.signature.replaceAll("\\*", "")).map(_._2._2).fold(isetEmpty)(iunion _)
  }
}

