/**
 * File: src/singleapp/ICCAnalysis.scala
 * -------------------------------------------------------------------------------------------
 * Date     Author      Changes
 * -------------------------------------------------------------------------------------------
 * 11/7/15   hcai       created; as the starter of ICC analysis producing ICC entry&exit points
*/

package singleapp

import org.sireum.amandroid.alir.pta.reachingFactsAnalysis.AndroidReachingFactsAnalysisConfig
import org.sireum.amandroid.security.AmandroidSocket
import org.sireum.util.FileUtil
import org.sireum.amandroid.security.interComponentCommunication.IccCollector
import org.sireum.amandroid.util.AndroidLibraryAPISummary
import org.sireum.amandroid.security.AmandroidSocketListener
import org.sireum.amandroid.AndroidGlobalConfig
import java.io.PrintWriter
import java.io.File
import org.sireum.amandroid.AndroidConstants
import org.sireum.jawa.util.SubStringCounter
import org.sireum.util.FileResourceUri
import org.sireum.jawa.util.IgnoreException
import org.sireum.jawa.util.MyTimer
import org.sireum.jawa.util.MyTimeoutException
import org.sireum.jawa.Global
import org.sireum.amandroid.Apk
import org.sireum.jawa.PrintReporter
import org.sireum.jawa.MsgLevel

object ICCAnalysis {
  private final val TITLE = "ICC entry-exit and flow analysis"
  object ICCStat {
    var iccTotal = 0
    var foundIccContainer = 0
    var totalEntry = 0
    var totalExit = 0
    
    var output_path = ""
    
    override def toString: String = {
    		"iccTotal: " + iccTotal + ", foundIccContainer: " + foundIccContainer + 
        ", totalEntry: " + totalEntry + ", totalExit: " + totalExit
    }
    
    def reset = {
      this.iccTotal = 0
      this.foundIccContainer = 0
      this.iccTotal = 0
      this.totalEntry = 0
      this.totalExit = 0
    }
  }
  private class taskListener(global: GlobalEx, apk: Apk, app_info: IccCollector) extends AmandroidSocketListener {
    def onPreAnalysis: Unit = {
      val iccSigs = AndroidConstants.getIccMethods()
      val codes = global.getApplicationClassCodes
      codes.foreach{
        case (rName, source) =>
          ICCStat.iccTotal += iccSigs.map(sig => SubStringCounter.countSubstring(source.code, sig + " @classDescriptor")).reduce((i, j) => i + j)
      }
    }

    def entryPointFilter(eps: Set[org.sireum.jawa.JawaMethod]): Set[org.sireum.jawa.JawaMethod] = {
      val iccContainerExt = global.getApplicationClasses.filter{
        appcls =>
          appcls.getMethods.exists { 
            me => 
            //System.out.println(me)
            //System.out.println("\t\t" + me.retrieveCode.getOrElse(""))
            me.retrieveCode.getOrElse("").contains(settings.config.GET_INTENT)
          }
      }.toSet
      val res = eps.filter(e=>
        app_info.getIccContainers.contains(e.getDeclaringClass) || 
        iccContainerExt.contains(e.getDeclaringClass)
      )
      if(!res.isEmpty){
        ICCStat.foundIccContainer += 1
      }
      res
    }

    def onTimeout: Unit = {}

    def onAnalysisSuccess: Unit = {
      val appData = DataStore.collect(global, apk)
      MetricStat.collect(appData)
      //val outputDir = AndroidGlobalConfig.amandroid_home + "/output"
      val outputDir = ICCStat.output_path + "/../output"
      val apkName = apk.nameUri.substring(apk.nameUri.lastIndexOf("/"), apk.nameUri.lastIndexOf("."))
      val appDataDirFile = new File(outputDir + "/" + apkName)
      if(!appDataDirFile.exists()) appDataDirFile.mkdirs()
      println(appData.toString())
      val out = new PrintWriter(appDataDirFile + "/iccData.txt")
      out.print(appData.toString)
      out.close()
      val mr = new PrintWriter(outputDir + "/iccMetric.txt")
      mr.print(MetricStat.toString)
      mr.close()
    }

    def onPostAnalysis: Unit = {
      global.reporter.echo(TITLE, ICCStat.toString)
    }
    
    def onException(e: Exception): Unit = {
      e match{
        case ie: IgnoreException => System.err.println("Ignored!")
        case a => 
          e.printStackTrace()
      }
    }
  }
  
  def main(args: Array[String]): Unit = {
    if(args.size < 2){
      System.err.print("Usage: source_path output_path [dependence_path]")
      return
    }
    
//    GlobalConfig.ICFG_CONTEXT_K = 1
    AndroidReachingFactsAnalysisConfig.resolve_icc = false
    AndroidReachingFactsAnalysisConfig.resolve_static_init = false
//    AndroidReachingFactsAnalysisConfig.timeout = 10
    
    val sourcePath = args(0)
    val outputPath = args(1)
    val dpsuri = try{Some(FileUtil.toUri(args(2)))} catch {case e: Exception => None}
    val files = FileUtil.listFiles(FileUtil.toUri(sourcePath), ".apk", true).toSet
    
    ICCStat.output_path = outputPath
    
    files.foreach{
      file =>
        val reporter = new PrintReporter(MsgLevel.ERROR)
        val global = new GlobalEx(file, reporter)
        global.setJavaLib(settings.config.classpath)
        val apk = new Apk(file)
        val socket = new AmandroidSocketEx(global, apk)
        try{
          ICCStat.reset
          reporter.echo(TITLE, subTask(global, apk, outputPath, dpsuri, file, socket, Some(500)).run)   
        } catch {
          case te: MyTimeoutException => reporter.error(TITLE, te.message)
          case e: Throwable => e.printStackTrace()
        } finally{
          socket.cleanEnv
        }
    }
  }
  
  private case class subTask(global: GlobalEx, apk: Apk, outputPath: String, dpsuri: Option[FileResourceUri], file: FileResourceUri, socket: AmandroidSocketEx, timeout: Option[Int]){
    def run: String = {
      global.reporter.echo(TITLE, "####" + file + "#####")
      val timer = timeout match {
        case Some(t) => Some(new MyTimer(t))
        case None => None
      }
      if(timer.isDefined) timer.get.start
      val outUri = socket.loadApk(outputPath, AndroidLibraryAPISummary, dpsuri, false, false)
      //global.resetLibs
      
      val app_info = new IccCollector(global, apk, outUri, timer)
      app_info.collectInfo
      socket.plugListener(new taskListener(global, apk, app_info))
      //socket.runWithoutDDA(false, true, timer)
      val ssm = new SourceAndSinkManagerStrict(global, apk, app_info.getLayoutControls, app_info.getCallbackMethods, AndroidGlobalConfig.SourceAndSinkFilePath)
      socket.runWithDDAEx(ssm, false, true, timer)
      return "Done ICC Analysis!"
    }
  }
}
