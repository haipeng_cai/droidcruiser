/**
 * File: src/singleapp/AndroidTaintAnalysisEx.scala
 * -------------------------------------------------------------------------------------------
 * Date     Author      Changes
 * -------------------------------------------------------------------------------------------
 * 11/12/15   hcai      created; customize AndroidDataDependentTaintAnalysis for better performance 
 *                      (avoid computing taint paths when only the sources and sinks are needed)
*/
package singleapp

import org.sireum.jawa.alir.dataDependenceAnalysis._
import org.sireum.amandroid.alir.pta.reachingFactsAnalysis.AndroidReachingFactsAnalysis
import org.sireum.util._
import org.sireum.jawa.JawaMethod
import org.sireum.jawa.alir.controlFlowGraph._
import org.sireum.pilar.ast._
import org.sireum.jawa.alir.dataDependenceAnalysis.InterproceduralDataDependenceInfo
import org.sireum.jawa.alir.taintAnalysis._
import org.sireum.amandroid.security.AndroidProblemCategories
import scala.tools.nsc.ConsoleWriter
import org.sireum.jawa.alir.pta.PTAResult
import org.sireum.jawa.alir.pta.VarSlot
import org.sireum.jawa.alir.pta.reachingFactsAnalysis.ReachingFactsAnalysisHelper
import org.sireum.jawa.Signature
import org.sireum.jawa.Global
import org.sireum.jawa.alir.taintAnalysis.TaintSource
import org.sireum.jawa.alir.taintAnalysis.TaintSink
import org.sireum.amandroid.alir.taintAnalysis._

object AndroidTaintAnalysisEx {
  final val TITLE = "AndroidTaintAnalysisEx"
  type Node = InterproceduralDataDependenceAnalysis.Node
  
  case class Tar(iddi: InterproceduralDataDependenceInfo) extends TaintAnalysisResult[Node, InterproceduralDataDependenceAnalysis.Edge] {
    var sourceNodes: ISet[TaintSource[Node]] = isetEmpty
    var sinkNodes: ISet[TaintSink[Node]] = isetEmpty
    
    def getSourceNodes: ISet[TaintNode[Node]] = this.sourceNodes.toSet
    def getSinkNodes: ISet[TaintNode[Node]] = this.sinkNodes.toSet
    def getTaintedPaths: ISet[TaintPath[Node, InterproceduralDataDependenceAnalysis.Edge]] = {
      isetEmpty
    }
    override def toString: String = {
      val sb = new StringBuilder
      val paths = getTaintedPaths
      if(!paths.isEmpty){
        getTaintedPaths.foreach(tp => sb.append(tp.toString) + "\n")
      }
      sb.toString.intern()
    }
  }
    
  def apply(global: Global, iddi: InterproceduralDataDependenceInfo, ptaresult: PTAResult, ssm: AndroidSourceAndSinkManager): TaintAnalysisResult[Node, InterproceduralDataDependenceAnalysis.Edge]
    = build(global, iddi, ptaresult, ssm) 
  
  def build(global: Global, iddi: InterproceduralDataDependenceInfo, ptaresult: PTAResult, ssm: AndroidSourceAndSinkManager): TaintAnalysisResult[Node, InterproceduralDataDependenceAnalysis.Edge] = {
    var sourceNodes: ISet[TaintSource[Node]] = isetEmpty
    var sinkNodes: ISet[TaintSink[Node]] = isetEmpty
    
    val iddg = iddi.getIddg
    iddg.nodes.foreach{
      node =>
        val (src, sin) = ssm.getSourceAndSinkNode(node, ptaresult)
        sourceNodes ++= src
        sinkNodes ++= sin
    }
    sinkNodes foreach {
      sinN =>
        if(sinN.node.isInstanceOf[IDDGCallArgNode])
          extendIDDGForSinkApis(iddg, sinN.node.asInstanceOf[IDDGCallArgNode], ptaresult)
    }
    val tar = Tar(iddi)
    tar.sourceNodes = sourceNodes
    tar.sinkNodes = sinkNodes
    
    tar
  }
  
  private def extendIDDGForSinkApis(iddg: DataDependenceBaseGraph[InterproceduralDataDependenceAnalysis.Node], callArgNode: IDDGCallArgNode, ptaresult: PTAResult) = {
    val calleeSet = callArgNode.getCalleeSet
    calleeSet.foreach{
      callee =>
        val argSlot = VarSlot(callArgNode.argName, false, true)
        val argValue = ptaresult.pointsToSet(argSlot, callArgNode.getContext)
        val argRelatedValue = ptaresult.getRelatedHeapInstances(argValue, callArgNode.getContext)
        argRelatedValue.foreach{
          case ins =>
            if(ins.defSite != callArgNode.getContext){
              val t = iddg.findDefSite(ins.defSite)
              iddg.addEdge(callArgNode.asInstanceOf[Node], t)
            }
        }
    }
  }
  
}
