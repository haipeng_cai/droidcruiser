/**
 * File: src/singleapp/GlobalEx.scala
 * -------------------------------------------------------------------------------------------
 * Date     Author      Changes
 * -------------------------------------------------------------------------------------------
 * 11/12/15   hcai      created; customize Global for performance (avoid loading 3-party libraries)
*/
package singleapp

import org.sireum.jawa.Global
import org.sireum.jawa.Reporter
import org.sireum.util._
import org.sireum.jawa.backend.JavaPlatform
import org.sireum.jawa.classpath.Classpath
import org.sireum.jawa.classpath.ClassFileLookup
import org.sireum.jawa.classpath.FlatClasspath
import org.sireum.jawa.classpath.ClassRepresentation
import org.sireum.jawa.util.MyFileUtil
import org.sireum.jawa.io.FgSourceFile
import org.sireum.jawa.io.PlainFile
import org.sireum.jawa.io.SourceFile
import org.sireum.jawa.sourcefile.SourcefileParser
import org.sireum.jawa.classfile.ClassfileParser
import org.sireum.jawa.LibraryAPISummary
import org.sireum.jawa.ObjectType
import org.sireum.jawa.LightWeightPilarParser
import org.sireum.jawa.JavaKnowledge

class GlobalEx(val _projectName: String, val _reporter: Reporter) extends Global (_projectName, _reporter) {
  private final val TITLE = "GlobalEx"
  
  override def load(fileRootUri: FileResourceUri, ext: String, summary: LibraryAPISummary) = {
    val fileUris = FileUtil.listFiles(fileRootUri, ext, true)
    fileUris.foreach{
      fileUri =>
        
        val aspkgname = fileUri.substring(fileUri.indexOf("src/")+4).replace('/', '.')
        if (summary.isLibraryAPI(aspkgname)) {
            System.out.println("will skip loading "+fileUri)
        }
        else { 
        	val source = new FgSourceFile(new PlainFile(FileUtil.toFile(fileUri)))
        	val codes = source.getClassCodes
        	val classTypes: MSet[ObjectType] = msetEmpty
        	codes.foreach{
        		code =>
        		try {
        			val className = LightWeightPilarParser.getClassName(code)
        					classTypes += JavaKnowledge.getTypeFromName(className).asInstanceOf[ObjectType]
        		} catch {
        		case e: Exception => reporter.warning(TITLE, e.getMessage)
        		}
        	}
        	classTypes.foreach {
        		typ =>
        		summary.isLibraryClass(typ) match {
        		case true => this.userLibraryClassCodes(typ) = source
        		case false => this.applicationClassCodes(typ) = source
        		}
        	}
        }
    }
  }
  
  
  def resetLibs = {
    this.systemLibraryClasses.clear()
    this.userLibraryClasses.clear()
    this.userLibraryClassCodes.clear()
  }
}