/**
 * File: src/settings/userTrigger.scala
 * -------------------------------------------------------------------------------------------
 * Date     Author      Changes
 * -------------------------------------------------------------------------------------------
 * 11/11/15	hcai      	created; signatures of known user triggers
*/
package settings

object userTriggerAPIs {   
    
    var usertriggers = Set[String]()
    var sensitiveAPIs = Set[String]()
    
    def apply : (Set[String], Set[String])= {
    	init_usertriggers
      init_sensitiveAPIs
      (usertriggers, sensitiveAPIs)
    }
    
    def isUserTrigger (signature : String) : Boolean = {
      if (usertriggers.isEmpty) init_usertriggers
      usertriggers.foreach {
        trigger => 
          if (signature.contains(trigger)) return true 
      }
      false
    }
    
    def init_usertriggers = {
                
        usertriggers+=("getText:()Landroid/text/Editable;"); //Whether it is editText or textView

        usertriggers+=("onClick:()V");
        usertriggers+=("onListItemClick:()V"); // it is a method that will be called when an item in the list is selected.
        usertriggers+=("onOptionsItemSelected:()Z"); // for menu item click
        usertriggers+=("onMenuItemClick:()Z"); // for pop up menu item click
        usertriggers+=("onContextItemSelected:()Z"); 
        
        usertriggers+=("onKey:()Z");
        usertriggers+=("onKeyDown:()Z");
        usertriggers+=("onKeyLongPress:()Z");
        usertriggers+=("onTouch:()Z"); // ok lets+= it. no need to consider this as it touches on view/screen which is useless and malware can exploit this to perform malicious action
        
        usertriggers+=("onItemSelected:()V"); // for spinner UI object
        usertriggers+=("onRadioButtonClicked:()V"); // for RadioButton object
        usertriggers+=("onToggleClicked:()V"); // for ToggleButton object
        usertriggers+=("onCheckboxClicked:()V"); // for CheckBox object
        usertriggers+=("onPictureTaken:()V"); // hardware event handler click for camera

        usertriggers+=("onLongClick:()Z");
        usertriggers+=("onItemLongClick:()Z");
        usertriggers+=("performClick:()Z");
        usertriggers+=("performLongClick:()Z");
        usertriggers+=("performItemClick:()Z");
        
        usertriggers+=("onTabChanged:()V");
        usertriggers+=("onEditorAction:()Z");
        
        usertriggers+=("onTabSelected:()V"); // ActionBar.TabListener callbacks
        usertriggers+=("onTabReselected:()V"); // ActionBar.TabListener callbacks
        
        usertriggers+=("onCheckedChanged:()V"); //for Toggle Buttons
        usertriggers+=("onDrag:()Z"); //for drag event
        
        usertriggers+=("onRatingChanged:()V"); // for rating bar 
        usertriggers+=("onClose:()Z"); // close search view
        
        usertriggers+=("onLocationChanged:()V"); // triggered based on user movement with device
        
        usertriggers+=("onProgressChanged:()V"); // progress bar level

        usertriggers+=("onPreferenceClick:()Z"); // Called when a Preference has been clicked
        
        usertriggers+=("onItemClick:()V"); // a callback to be invoked when an item in this AdapterView has been clicked
        
        usertriggers+=("onQueryTextChange:()Z"); // Called when the query text is changed by the user
        usertriggers+=("onQueryTextSubmit:()Z"); // Called when the user submits the query.
        
        usertriggers+=("onTextChanged:()V"); // a callback for TextWatcher
        usertriggers+=("afterTextChanged:()V"); // a callback for TextWatcher
   }        
    
   def isSensitiveAPI (signature : String) : Boolean = {
      if (sensitiveAPIs.isEmpty) init_sensitiveAPIs
      sensitiveAPIs.foreach {
        sensapi => 
          if (signature.contains(sensapi)) return true 
      }
      false
    } 
                
   def init_sensitiveAPIs = {
    	// those APIs for inter- and intra- app communication, only consider those that are taking Intent as argument
    	/*
    	sensitiveAPIs+=("startActivity(");
    	sensitiveAPIs+=("startActivities(");
    	sensitiveAPIs+=("startActivityForResult(");
    	sensitiveAPIs+=("startActivityFromChild(");
    	sensitiveAPIs+=("startActivityFromFragment(");
    	sensitiveAPIs+=("startActivityIfNeeded(");
    	sensitiveAPIs+=("startNextMatchingActivity(");	
    			
    	sensitiveAPIs+=("sendIntent(");
    	sensitiveAPIs+=("startIntentSender(");
    	sensitiveAPIs+=("startIntentSenderForResult(");
    	sensitiveAPIs+=("startIntentSenderFromChild(");
    	
    	sensitiveAPIs+=("sendBroadcast(");
    	sensitiveAPIs+=("sendBroadcastAsUser(");    	
    	sensitiveAPIs+=("sendOrderedBroadcast(");
    	sensitiveAPIs+=("sendOrderedBroadcastAsUser(");   	
    	sensitiveAPIs+=("sendStickyBroadcast(");
    	sensitiveAPIs+=("sendStickyBroadcastAsUser(");   	
    	sensitiveAPIs+=("sendStickyOrderedBroadcast(");
    	sensitiveAPIs+=("sendStickyOrderedBroadcastAsUser(");    	
    	sensitiveAPIs+=("removeStickyBroadcast(");
    	sensitiveAPIs+=("removeStickyBroadcastAsUser(");

    	sensitiveAPIs+=("bindService(");
    	sensitiveAPIs+=("startService(");
    	sensitiveAPIs+=("stopService(");
    	*/
    	
    	
        // note: no need to use "query" operation as sensitive function
        sensitiveAPIs+=("android.content.ContentResolver: android.net.Uri insert");
        sensitiveAPIs+=("android.content.ContentResolver: int delete");
        sensitiveAPIs+=("android.content.ContentResolver: int update");

        sensitiveAPIs+=("android.database.sqlite.SQLiteDatabase: long insert");
        sensitiveAPIs+=("android.database.sqlite.SQLiteDatabase: long insertOrThrow");       
        sensitiveAPIs+=("android.database.sqlite.SQLiteDatabase: int delete");        
        sensitiveAPIs+=("android.database.sqlite.SQLiteDatabase: int update");     
        //sensitiveAPIs+=("android.database.sqlite.SQLiteDatabase: void execSQL"); // no need, it does not take input
        
        sensitiveAPIs+=("android.telephony.gsm.SmsManager: void sendTextMessage");   
        sensitiveAPIs+=("android.telephony.SmsManager: void sendTextMessage");  
        sensitiveAPIs+=("android.telephony.SmsManager: void sendDataMessage"); 
        sensitiveAPIs+=("android.telephony.SmsManager: void sendMultipartTextMessage"); 
        
        sensitiveAPIs+=("java.io.FileOutputStream: void write");
        sensitiveAPIs+=("java.io.OutputStream: void write");
        sensitiveAPIs+=("java.io.BufferedWriter: void write");
        
        sensitiveAPIs+=("java.io.FileInputStream: int read");
        sensitiveAPIs+=("java.io.InputStream: int read");
        sensitiveAPIs+=("java.io.BufferedReader: java.lang.String readLine");
        sensitiveAPIs+=("java.io.BufferedReader: int read");

        sensitiveAPIs+=("java.io.FileInputStream openFileInput");
        sensitiveAPIs+=("java.io.FileOutputStream openFileOutput");    
        
        sensitiveAPIs+=("java.io.File: boolean createNewFile");    
        sensitiveAPIs+=("java.io.File: java.io.File createTempFile");    
        sensitiveAPIs+=("java.io.File: boolean delete");    
        sensitiveAPIs+=("java.io.File: void deleteOnExit"); 

        sensitiveAPIs+=("boolean deleteDatabase"); 
        sensitiveAPIs+=("boolean deleteFile"); 
 
        //Webview object
        // sensitiveAPIs+=("android.webkit.WebView: void loadUrl"); 
        // sensitiveAPIs+=("android.webkit.WebView: void loadData");
        //sensitiveAPIs+=("android.webkit.WebView: void postUrl");
        
        sensitiveAPIs+=("java.lang.Runtime: java.lang.Process exec");
                
        // sensitiveAPIs+=("java.net.HttpURLConnection: void connect"); // no parameter
        // sensitiveAPIs+=("java.net.URL: java.net.URLConnection openConnection"); no parameter
        
        // sensitiveAPIs+=("java.net.URLConnection: java.io.InputStream getInputStream");
        sensitiveAPIs+=("java.io.InputStreamReader: int read"); // java.net.HttpURLConnection: java.io.InputStream getInputStream()
        
        
        /* no need for data input and output stream, it reads/writes Java data types
        sensitiveAPIs+=("java.io.DataInputStream: void read");
        sensitiveAPIs+=("java.io.DataInputStream: int read");
        sensitiveAPIs+=("java.io.DataInputStream: double read");
        sensitiveAPIs+=("java.io.DataInputStream: byte read");
        sensitiveAPIs+=("java.io.DataInputStream: char read");
        sensitiveAPIs+=("java.io.DataInputStream: long read");
        sensitiveAPIs+=("java.io.DataInputStream: short read");
        sensitiveAPIs+=("java.io.DataInputStream: java.lang.String read");
        
        sensitiveAPIs+=("java.io.DataOutputStream: void write");
        */
        
        
        // sensitiveAPIs+=("java.net.URLConnection: java.io.OutputStream getOutputStream");
        sensitiveAPIs+=("java.io.OutputStreamWriter: void write"); // java.net.HttpURLConnection: java.io.OutputStream getOutputStream()
           
        
        sensitiveAPIs+=("org.apache.http.impl.client.DefaultHttpClient: org.apache.http.HttpResponse execute");
        sensitiveAPIs+=("org.apache.http.impl.client.DefaultHttpClient: java.lang.Object execute");
        sensitiveAPIs+=("org.apache.http.client.HttpClient: java.lang.Object execute");
        sensitiveAPIs+=("android.net.http.AndroidHttpClient: java.lang.Object execute");
        sensitiveAPIs+=("android.net.http.AndroidHttpClient: org.apache.http.HttpResponse execute");
        
        // sensitiveAPIs+=("java.net.Socket");
        sensitiveAPIs+=("java.net.Socket: void sendUrgentData"); // I would like to extract socket operations
        sensitiveAPIs+=("java.net.DatagramSocket: void receive"); // java.net.DatagramSocket
        sensitiveAPIs+=("java.net.DatagramSocket: void send"); // java.net.DatagramSocket
             
        // access of location APIs
        sensitiveAPIs+=("getLastKnownLocation");
        sensitiveAPIs+=("android.location.Location: float getAccuracy");
        sensitiveAPIs+=("android.location.Location: double getAltitude");
        sensitiveAPIs+=("android.location.Location: double getLongitude");
        sensitiveAPIs+=("android.location.Location: double getLatitude");
       
        // access of phone identifier APIs
        sensitiveAPIs+=("getCellLocation");       
        sensitiveAPIs+=("android.telephony.TelephonyManager: java.lang.String getDeviceId");       
        sensitiveAPIs+=("android.telephony.TelephonyManager: java.lang.String getDeviceSoftwareVersion");       
        sensitiveAPIs+=("android.telephony.TelephonyManager: java.lang.String getLine1Number");       
        sensitiveAPIs+=("android.telephony.TelephonyManager: java.lang.String getNetworkOperator");       
        sensitiveAPIs+=("android.telephony.TelephonyManager: java.lang.String getNetworkOperatorName()");       
        sensitiveAPIs+=("android.telephony.TelephonyManager: int getPhoneType");       
        sensitiveAPIs+=("android.telephony.TelephonyManager: java.lang.String getSimOperatorName");
        sensitiveAPIs+=("android.telephony.TelephonyManager: java.lang.String getSimSerialNumber");
        sensitiveAPIs+=("android.telephony.TelephonyManager: java.lang.String getSubscriberId");
        
        // access of wifi data APIs
        sensitiveAPIs+=("android.net.wifi.WifiInfo: java.lang.String getBSSID");
        sensitiveAPIs+=("android.net.wifi.WifiInfo: int getIpAddress");
        sensitiveAPIs+=("android.net.wifi.WifiInfo: java.lang.String getMacAddress");
        sensitiveAPIs+=("android.net.wifi.WifiInfo: int getNetworkId");
        sensitiveAPIs+=("android.net.wifi.WifiInfo: java.lang.String getSSID");
        sensitiveAPIs+=("android.net.wifi.WifiInfo: java.lang.String getSubscriberId");
    }
}
