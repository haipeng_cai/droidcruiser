group AppData;

delimiters "$", "$"

AppData(name, uses_permissions, components, dynamicRegisteredComponents) ::= <<
Application Name: $name$
Uses Permissions: $uses_permissions ; separator=", "$

  $components ; separator="\n\n"$
$if(dynamicRegisteredComponents)$
Dynamic Registered Components:
  $dynamicRegisteredComponents ; separator="\n\n"$
$endif$
>>

ComponentData(compName, typ, exported, protectPermission, intentFilters, outIccInfos, inIccInfos, taintResult) ::= <<
############################### Per-Component Facts start ######################################
  Component name:    $compName$
  Component type:    $typ$
  Whether-Exported:  $exported$
  Access-Permission: $protectPermission ; separator=", "$
  IntentFilters:
    $intentFilters ; separator="\n"$
  **************************** Outgoing ICCs *********************************
  $outIccInfos ; separator="\n\n"$
  **************************** Incoming ICCs *********************************
  $inIccInfos ; separator="\n\n"$
  
  $if(taintResult)$
  **************************** taint flow *********************************
  $taintResult$
  *************************************************************
  $endif$
############################### Per-Component Facts end ######################################

>>

DynamicRegisteredComponentData(compName, typ, protectPermission, intentFilters) ::= <<
Dynamic registered component: $compName$
  typ: $typ$
  Access Permission: $protectPermission$
  IntentFilters:
    $intentFilters ; separator="\n"$
>>

IntentFilter(actions, categories, data) ::= <<
IntentFilter:($if(actions)$Actions:["$actions ; separator="\",\""$"]$endif$$if(categories)$,Categories:["$categories ; separator="\",\""$"]$endif$$if(data)$,Data:[$data$]$endif$)
>>

Data(schemes, hosts, ports, paths, pathPrefixs, pathPatterns, mimeTypes) ::= <<
($if(schemes)$Schemes:<"$schemes ; separator="\",\""$">,$endif$$if(hosts)$Hosts:<"$hosts ; separator="\",\""$">,$endif$$if(ports)$Ports:<"$ports ; separator="\",\""$">,$endif$$if(paths)$Paths:<"$paths ; separator="\",\""$">,$endif$$if(pathPrefixs)$PathPrefixs:<"$pathPrefixs ; separator="\",\""$">,$endif$$if(pathPatterns)$PathPatterns:<"$pathPatterns ; separator="\",\""$">,$endif$$if(mimeTypes)$MimeTypes:<"$mimeTypes ; separator="\",\""$">$endif$)
>>

OutIccInfo(procs, context, intents, reachableTaintSources, reachableUserTriggers) ::= <<
  Calling Procedure: $procs ; separator="\n"$
  Calling Context:   $context$
  Outgoing Intents:
    $intents ; separator="\n"$
  Backward-Reachable taint sources:
  	$reachableTaintSources ; separator="\n"$
  Backward-Reachable user triggers:
 	$reachableUserTriggers ; separator="\n"$
>>

InIccInfo(procs, context, intents, reachableTaintSinks) ::= <<
  Receiving Procedure: $procs ; separator="\n"$
  Procedure Context:   $context$
  Forward-Reachable taint sinks:
  	$reachableTaintSinks ; separator="\n"$
>>

Intent(componentNames, actions, categories, datas, typs, targets) ::= <<
Intent:
  $if(componentNames)$mComponentNames:
  "$componentNames ; separator="\"\n  \""$"$endif$

  $if(actions)$mActions:
  "$actions ; separator="\"\n  \""$"$endif$

  $if(categories)$mCategories:
  "$categories ; separator="\"\n  \""$"$endif$

  $if(datas)$mDatas:
  $datas ; separator="\"\n  \""$$endif$

  $if(typs)$mimeTypes:
  "$typs ; separator="\"\n  \""$"$endif$

  ICC destinations:
    $targets ; separator="\n"$
>>

UriData(scheme, host, port, path, pathPrefix, pathPattern) ::= <<
<Scheme:"$scheme$",Host:"$host$",Port:"$port$",Path:"$path$",PathPrefix:"$pathPrefix$",PathPattern:"$pathPattern$">
>>

TaintResult(sources, sinks, paths) ::= <<
Sources found:
  $sources ; separator="\n"$
Sinks found:
  $sinks ; separator="\n"$
Discovered taint paths are listed below:
  $paths ; separator="\n"$
>>

Target(proc, typ) ::= <<
Target Component: $proc$, Intent Type: $typ$
>>

SourceSinkInfo(descriptors) ::= <<
<Descriptors: $descriptors ; separator=" "$>
>>

TaintPath(source, sink, typs, path) ::= <<
TaintPath:
  Source: $source$
  Sink: $sink$
  Types: $typs ; separator=", "$
  The path consists of the following edges ("->"). The nodes have the context information. The source is at the top :
    $path ; separator="\n"$
>>
