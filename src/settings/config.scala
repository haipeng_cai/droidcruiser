/**
 * File: src/settings/config.scala
 * -------------------------------------------------------------------------------------------
 * Date     Author      Changes
 * -------------------------------------------------------------------------------------------
 * 11/7/15   hcai       created; as the central place holding configurations
*/
package settings

object config {
  final val classpath = "/home/hcai/Android/Sdk/platforms/android-21/android.jar:/home/hcai/Android/Sdk/extras/android/support/v4/android-support-v4.jar:/home/hcai/Android/Sdk//extras/android/support/v13/android-support-v13.jar"
  final val GET_INTENT = "getIntent:()Landroid/content/Intent;"
}